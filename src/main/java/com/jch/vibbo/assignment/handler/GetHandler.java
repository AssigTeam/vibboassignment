package com.jch.vibbo.assignment.handler;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.http.HttpStatus;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public abstract class GetHandler implements HttpHandler {	
	
	/**
	 * Use to send an Ok response to the client.
	 * 
	 * @param exchange http request and response encapsulation
	 * @param response message or page to display
	 * @throws IOException if any issue with the outputStream and the sendResponse
	 */
	protected void sendStatusOk(HttpExchange exchange, String response) throws IOException {
		exchange.sendResponseHeaders(HttpStatus.SC_OK, response.length());
        OutputStream os = exchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }
	
	/**
	 * 
	 * @param exchange http request and response encapsulation
	 * @param userName user connected
	 * @throws IOException if any issue with the outputStream and the sendResponse
	 */
	protected void sendForbiddenAcces(HttpExchange exchange, String userName) throws IOException {
        String response = getErrorHTML(userName);
        exchange.sendResponseHeaders(HttpStatus.SC_FORBIDDEN, response.length());
        OutputStream os = exchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }
	
	/**
	 * Basic error page to display in case of not authorized acces.
	 * @param name name of user connected
	 * @return html page as string
	 * @throws IOException if any issue with the load of the file.
	 */
	private String getErrorHTML(String name) throws IOException {
		String content;
        ClassLoader classLoader = getClass().getClassLoader();
		String filePath = classLoader.getResource("error.html").getPath();
		content = new String(Files.readAllBytes(Paths.get(System.getProperty("os.name").contains("indow") ? filePath.substring(1) : filePath)));
        content = content.replace("{name}", name);
        return content;
    }
	
	/**
	 * Method use to convert as string the webpage to display.
	 * @param userName name of user connected
	 * @return webpage as string.
	 */
	protected abstract String getIndexHTML(String userName);
}
