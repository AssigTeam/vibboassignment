package com.jch.vibbo.assignment.handler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.jch.vibbo.assignment.bean.RolesUser;
import com.jch.vibbo.assignment.service.UserService;
import com.jch.vibbo.assignment.utils.Utils;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * Handler responsible of the admin actions.
 * Display a web page with three forms : Create, Update, Delete.
 * 
 * @author Jérémy
 *
 */
public class AdminActionHandler extends GetHandler implements HttpHandler {

private UserService userService;
	
	public AdminActionHandler(UserService service) {
		this.userService = service;
	}

	public void handle(HttpExchange httpExchange) throws IOException {
		
		String userName = Utils.getUserNameOfSession(httpExchange);
        boolean validRole = userService.validateUserRole(userName, RolesUser.ADMIN);
        if (validRole) {
            String response = getIndexHTML(userName);
            sendStatusOk(httpExchange, response);
        } else {
            sendForbiddenAcces(httpExchange, userName);
        }
    }

	@Override
	protected String getIndexHTML(String userName) {
	        String content;
	        ClassLoader classLoader = getClass().getClassLoader();
			String filePath = classLoader.getResource("adminAction.html").getPath();
			try {
				content = new String(Files.readAllBytes(Paths.get(System.getProperty("os.name").contains("indow") ? filePath.substring(1) : filePath)));
		        return content;
			} catch (IOException e) {
				e.printStackTrace();
			}
			return "";
	    }

}
