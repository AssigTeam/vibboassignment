package com.jch.vibbo.assignment.handler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * Handler responsible of the login part.
 * @author Jérémy
 *
 */
public class LoginHandler extends GetHandler implements HttpHandler {

	@Override
    public void handle(HttpExchange t) throws IOException {
        String response = getIndexHTML(null);
        sendStatusOk(t, response);
    }

	@Override
	protected String getIndexHTML(String userName) {
	        String content;
	        ClassLoader classLoader = getClass().getClassLoader();
			String filePath = classLoader.getResource("login.html").getPath();
			try {
				content = new String(Files.readAllBytes(Paths.get(System.getProperty("os.name").contains("indow") ? filePath.substring(1) : filePath)));
		        return content;
			} catch (IOException e) {
				e.printStackTrace();
			}
			return "";
	    }

}
