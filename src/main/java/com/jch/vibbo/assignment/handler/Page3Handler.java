
package com.jch.vibbo.assignment.handler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.jch.vibbo.assignment.bean.RolesUser;
import com.jch.vibbo.assignment.service.UserService;
import com.jch.vibbo.assignment.utils.Utils;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * Handler for Page3
 * @author Jérémy
 *
 */
public class Page3Handler extends GetHandler implements HttpHandler {

	private UserService userService;

	public Page3Handler(UserService service) {
		this.userService = service;
	}

	public void handle(HttpExchange httpExchange) throws IOException {

		String userName = Utils.getUserNameOfSession(httpExchange);
		boolean validRole = userService.validateUserRole(userName, RolesUser.PAGE_3);
		if (validRole) {
			String response = getIndexHTML(userName);
			sendStatusOk(httpExchange, response);
		} else {
			sendForbiddenAcces(httpExchange, userName);
		}
	}

	@Override
	protected String getIndexHTML(String userName) {
		ClassLoader classLoader = getClass().getClassLoader();
		String filePath = classLoader.getResource("page3.html").getPath();

		String content = "";
		try {
			content = new String(Files.readAllBytes(
					Paths.get(System.getProperty("os.name").contains("indow") ? filePath.substring(1) : filePath)));
			content = content.replace("{name}", userName);
			content = content.replace("{page}", RolesUser.PAGE_3.name());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return content;
	}

}
