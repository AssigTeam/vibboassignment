package com.jch.vibbo.assignment.handler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.jch.vibbo.assignment.utils.Utils;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * Handler responsible of the part after the login if the user use /login directly
 * @author Jérémy
 *
 */
public class LoggedHandler extends GetHandler implements HttpHandler {


	public void handle(HttpExchange httpExchange) throws IOException {
        String userName = Utils.getUserNameOfSession(httpExchange);
        String response = getIndexHTML(userName);
        sendStatusOk(httpExchange, response);
	}

	@Override
	protected String getIndexHTML(String userName) {
		String content;
        ClassLoader classLoader = getClass().getClassLoader();
		String filePath = classLoader.getResource("logged.html").getPath();
		try {
			content = new String(Files.readAllBytes(Paths.get(System.getProperty("os.name").contains("indow") ? filePath.substring(1) : filePath)));
	        content = content.replace("{name}", userName);
	        return content;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

}
