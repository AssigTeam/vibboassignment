package com.jch.vibbo.assignment.handler;

import java.io.IOException;

import com.jch.vibbo.assignment.service.SessionService;
import com.jch.vibbo.assignment.utils.Utils;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * Handler use for the logout part
 * 
 * @author Jérémy
 *
 */
public class LogOutHandler extends GetHandler implements HttpHandler {

	private SessionService sessionService;

    public LogOutHandler(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    public void handle(HttpExchange httpExchange) throws IOException {
        String sessionToken = Utils.getSessionToken(httpExchange);

        sessionService.invalidate(sessionToken);
        String response = getIndexHTML(null);
        sendStatusOk(httpExchange, response);
    }

	@Override
	protected String getIndexHTML(String userName) {
		return "{}";
	}

}
