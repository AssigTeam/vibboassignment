package com.jch.vibbo.assignment.handler;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.Map;

import org.apache.http.HttpStatus;
import org.json.JSONObject;

import com.jch.vibbo.assignment.bean.User;
import com.jch.vibbo.assignment.service.UserService;
import com.jch.vibbo.assignment.utils.Constantes;
import com.jch.vibbo.assignment.utils.HTTPMethods;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpPrincipal;

/**
 * Handler responsible of all admin actions and also the get action.
 * 
 * @author Jérémy
 *
 */
public class AdminPageHandler extends GetHandler implements HttpHandler{

	private UserService userService;
	
	public AdminPageHandler(UserService userService) {
		this.userService = userService;
	}

	@Override
	public void handle(HttpExchange httpExchange) throws IOException {
		 HttpPrincipal httpPrincipal = httpExchange.getPrincipal();
	        String requestMethod = httpExchange.getRequestMethod();
	        boolean validOperation = false;
	        if(!HTTPMethods.GET.name().equals(requestMethod)){

		        String userName = httpPrincipal.getName();
		        validOperation = userService.validateUserOperation(userName, requestMethod);
	        }else{
	        	validOperation = true;
	        }

	        if (validOperation) {
	            processAction(httpExchange, requestMethod);
	        } else {
	        	httpExchange.sendResponseHeaders(HttpStatus.SC_FORBIDDEN, -1L);
	        }
	}

	/**
	 * Process action, in two way the Get, and the other.
	 */
	private void processAction(HttpExchange httpExchange, String requestMethod) throws IOException {
	        if (HTTPMethods.GET.name().equalsIgnoreCase(requestMethod)) {
	            processGetOperation(httpExchange);
	        } else {
	            processOthersActions(httpExchange, requestMethod);
	        }
	    }
	
	/**
	 * meethod to perform the Get action
	 * @param httpExchange http request and response encapsulation
	 * @throws IOException throw if any issue comes from the creation of response.
	 */
	private void processGetOperation(HttpExchange httpExchange) throws IOException {
        Map<String, String> getParams = (Map<String, String>) httpExchange.getAttribute(Constantes.ATTR_PARAMETERS_MAP);
        User user = userService.findUserByUserName(getParams.get("userName"));
        sendGetResponse(httpExchange, user);
    }

	/**
	 * Creation of the response.
	 * @param httpExchange http request and response encapsulation
	 * @param user user searched
	 * @throws IOException throw if any issue comes from the creation of response.
	 */
    private void sendGetResponse(HttpExchange httpExchange, User user) throws IOException {
        if (user == null) {
        	httpExchange.sendResponseHeaders(HttpStatus.SC_NOT_FOUND, -1);
        } else {
        	httpExchange.getResponseHeaders().add(Constantes.CONTENT_TYPE_HEADER, Constantes.APPLICATION_JSON);
            sendStatusOk(httpExchange,getJsonMessage(user));
        }
    }
	
    /**
     * creation of JSon to reply.
     * @param user user searched by client
     * @return json as string
     */
	private String getJsonMessage(User user) {
		String message;
		JSONObject json = new JSONObject();
		json.put("username", user.getUserName());
		json.put("roles", String.join(",", user.getRoles()));

		message = json.toString();
		
		return message;
	}

	/**
	 * process others request (PUT, POST, DELETE)
	 * @param httpExchange  http request and response encapsulation
	 * @param requestMethod PUT POST or DELETE
	 * @throws IOException
	 */
	private void processOthersActions(HttpExchange httpExchange, String requestMethod) throws IOException {
		int httpCodeResult;
        String requestString = getStringFromHttpExchangeRequestBody(httpExchange);
        User user = new User(requestString);
        if (HTTPMethods.POST.name().equalsIgnoreCase(requestMethod)) {
            userService.insertUser(user);
            httpCodeResult = HttpStatus.SC_CREATED;
        } else if (HTTPMethods.PUT.name().equalsIgnoreCase(requestMethod)) {
            userService.updateUser(user);
            httpCodeResult = HttpStatus.SC_NO_CONTENT;
        } else if (HTTPMethods.DELETE.name().equalsIgnoreCase(requestMethod)) {
            userService.deleteUserByUserName(user.getUserName());
            httpCodeResult = HttpStatus.SC_NO_CONTENT;
        } else {
            httpCodeResult = HttpStatus.SC_NOT_FOUND;
        }
        httpExchange.sendResponseHeaders(httpCodeResult, -1);
		
	}
	
	/**
	 * Get request as string to determine parameters passed
	 * @param httpExchange
	 * @return httpRequest as string
	 */
	private String getStringFromHttpExchangeRequestBody(HttpExchange httpExchange) {
        String requestString = "";
        InputStream is;
        try {
            StringBuilder requestBuffer = new StringBuilder();
            is = httpExchange.getRequestBody();
            int rByte;
            while ((rByte = is.read()) != -1) {
                requestBuffer.append((char) rByte);
            }
            is.close();

            if (requestBuffer.length() > 0) {
                requestString = URLDecoder.decode(requestBuffer.toString(), "UTF_8");
            } else {
                requestString = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return requestString;
    }

	@Override
	protected String getIndexHTML(String userName) {
		// TODO Auto-generated method stub
		return null;
	}

}
