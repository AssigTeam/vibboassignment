package com.jch.vibbo.assignment.handler;

import java.io.IOException;
import java.util.UUID;

import com.jch.vibbo.assignment.service.SessionService;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpPrincipal;

/**
 * Handler responsible of the authentication (action login)
 * 
 * @author Jérémy
 *
 */
public class AuthenticateHandler extends GetHandler implements HttpHandler {

	private SessionService sessionService;

    public AuthenticateHandler(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    public void handle(HttpExchange t) throws IOException {
        HttpPrincipal httpPrincipal = t.getPrincipal();
        String userName = httpPrincipal.getName();

        String sessionToken = UUID.randomUUID().toString();
        sessionService.newSession(sessionToken, userName);
        String response = getIndexHTML(sessionToken);
        sendStatusOk(t, response);
    }

	@Override
	protected String getIndexHTML(String token) {
		return "{\"sessionToken\": \"" + token + "\"}";
	}

}
