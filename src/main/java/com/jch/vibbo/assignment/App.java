package com.jch.vibbo.assignment;

import java.io.IOException;

import com.jch.vibbo.assignment.authentication.WebAuthentication;
import com.jch.vibbo.assignment.factory.HandlerFactory;
import com.jch.vibbo.assignment.filter.ParameterFilter;
import com.jch.vibbo.assignment.filter.SessionFilter;
import com.jch.vibbo.assignment.repository.UserRepository;
import com.jch.vibbo.assignment.repository.impl.UserRepositoryLocal;
import com.jch.vibbo.assignment.service.SessionService;
import com.jch.vibbo.assignment.service.UserService;
import com.jch.vibbo.assignment.service.impl.SessionGuavaServiceImpl;
import com.jch.vibbo.assignment.service.impl.UserServiceImpl;
import com.jch.vibbo.assignment.utils.Constantes;


public class App implements Constantes{
	  public static void main(String[] args) throws IOException {
		  HandlerFactory factory = new HandlerFactory();

	      SessionService sessionService = new SessionGuavaServiceImpl();
		  UserRepository userRepository = new UserRepositoryLocal();
	      UserService userService = new UserServiceImpl(userRepository);
		  
		  BasicHttpServer basicHttpServer = BasicHttpServerBuilder
	                .aBasicHttpServer()
	                .withPort(9000)
	                .withAuthenticator(new WebAuthentication("", userService))
	                .withHandler(LOGIN, factory.getHandler(LOGIN, null))
	                .withHandlerAndFilter(LOGGED, factory.getHandler(LOGGED, null), new SessionFilter(sessionService))
	                .withHandlerAndFilter(LOGOUT, factory.getHandler(LOGOUT, sessionService), new SessionFilter(sessionService))
	                .withHandlerAndFilter(PAGE_1, factory.getHandler(PAGE_1, userService), new SessionFilter(sessionService))
	                .withHandlerAndFilter(PAGE_2, factory.getHandler(PAGE_2, userService), new SessionFilter(sessionService))
	                .withHandlerAndFilter(PAGE_3, factory.getHandler(PAGE_3, userService), new SessionFilter(sessionService))
	                .withHandler(AUTHENTICATE, factory.getHandler(AUTHENTICATE, sessionService), true)
	                .withHandlerAndFilter(USER_GET_INFO, factory.getHandler(USER_GET_INFO, null), new SessionFilter(sessionService))
	                .withHandlerAndFilter(USER_ADMIN_ACTION, factory.getHandler(USER_ADMIN_ACTION, userService), new SessionFilter(sessionService))
	                .withHandler(USER_ADMIN_DELETE, factory.getHandler(USER_ADMIN_DELETE, userService), true)
	                .withHandler(USER_ADMIN_UPDATE, factory.getHandler(USER_ADMIN_UPDATE, userService), true)
	                .withHandler(USER_ADMIN_INSERT, factory.getHandler(USER_ADMIN_INSERT, userService), true)
	                .withHandlerAndFilter(USER_INFO_USER, factory.getHandler(USER_INFO_USER, userService), new ParameterFilter())
	                .build();
		  
		  basicHttpServer.start();
		    System.out.println("The server is running");
		  }
	}