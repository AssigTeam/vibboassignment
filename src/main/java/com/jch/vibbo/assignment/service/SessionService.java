package com.jch.vibbo.assignment.service;

public interface SessionService {

    void newSession(String sessionToken, String userName);

    void invalidate(String sessionToken);
    
    String getUserNameOfSession(String sessionToken);
}
