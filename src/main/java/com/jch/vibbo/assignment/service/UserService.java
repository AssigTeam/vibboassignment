package com.jch.vibbo.assignment.service;

import com.jch.vibbo.assignment.bean.RolesUser;
import com.jch.vibbo.assignment.bean.User;

public interface UserService {

	void insertUser(User user);

    void updateUser(User user);

    void deleteUserByUserName(String userName);

    User findUserByUserName(String userName);

    boolean authenticateUser(String userName, String pwd);

    boolean validateUserRole(String userName, RolesUser userRole);

    boolean validateUserOperation(String userName, String requestMethod);
}
