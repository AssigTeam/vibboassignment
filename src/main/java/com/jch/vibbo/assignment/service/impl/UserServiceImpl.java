package com.jch.vibbo.assignment.service.impl;

import com.jch.vibbo.assignment.bean.RolesUser;
import com.jch.vibbo.assignment.bean.User;
import com.jch.vibbo.assignment.repository.UserRepository;
import com.jch.vibbo.assignment.service.UserService;

public class UserServiceImpl implements UserService {

	private UserRepository userRepository;
	
	public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
	@Override
	public void insertUser(User user) {
		userRepository.insert(user);		
	}

	@Override
	public void updateUser(User user) {
		userRepository.update(user);
	}

	@Override
	public void deleteUserByUserName(String userName) {
		userRepository.delete(userName);
	}

	@Override
	public User findUserByUserName(String userName) {
		return userRepository.findByUserName(userName);
	}

	@Override
	public boolean authenticateUser(String userName, String pwd) {
		User user = userRepository.findByUserName(userName);
		if (user != null && user.getPassword().equals(pwd)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean validateUserRole(String userName, RolesUser userRole) {

		User user = userRepository.findByUserName(userName);
        return user.getRoles().contains(RolesUser.ADMIN.toString()) || user.getRoles().contains(userRole.toString());
	}

	@Override
	public boolean validateUserOperation(String userName, String requestMethod) {
		User user = userRepository.findByUserName(userName);
        return user.getRoles().contains(RolesUser.ADMIN.toString()) || "GET".equals(requestMethod);
	}

}
