package com.jch.vibbo.assignment.service.impl;

import java.util.concurrent.TimeUnit;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.jch.vibbo.assignment.service.SessionService;

public class SessionGuavaServiceImpl implements SessionService {

    Cache<String, String> sessions;

    public SessionGuavaServiceImpl() {
        sessions = CacheBuilder.newBuilder()
                .maximumSize(10000)
                .expireAfterAccess(5, TimeUnit.MINUTES)
                .build();
    }
    
	public void newSession(String sessionToken, String userName) {
		sessions.put(sessionToken, userName);

	}

	public void invalidate(String sessionToken) {
		sessions.invalidate(sessionToken);
	}
	
	public String getUserNameOfSession(String sessionToken){
		if (sessionToken == null) return null;
        return sessions.getIfPresent(sessionToken);
	}

}
