package com.jch.vibbo.assignment;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


import com.sun.net.httpserver.Authenticator;
import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.HttpHandler;

/**
 * Builder responsible of the httpServer creation (builder pattern)
 * @author Jérémy
 *
 */
public class BasicHttpServerBuilder {
	private static HashMap<String, HttpHandler> handlers;
    private static HashMap<String, Filter> filters;
    private static HashMap<String, Boolean> authenticatedPaths;
    private Authenticator authenticator;
    private int port;

    private BasicHttpServerBuilder() {
    }

    public static BasicHttpServerBuilder aBasicHttpServer() {
        handlers = new HashMap<>();
        filters = new HashMap<>();
        authenticatedPaths = new HashMap<>();
        return new BasicHttpServerBuilder();
    }

    public BasicHttpServerBuilder withPort(int port) {
        this.port = port;
        return this;
    }

    public BasicHttpServerBuilder withHandler(String path, HttpHandler httpHandler, boolean auth) {
        handlers.put(path, httpHandler);
        authenticatedPaths.put(path, auth);
        return this;
    }

    public BasicHttpServerBuilder withHandler(String path, HttpHandler httpHandler) {
        return withHandler(path, httpHandler, false);
    }

    public BasicHttpServerBuilder withHandlerAndFilter(String path, HttpHandler httpHandler, Filter filter, boolean auth) {
        handlers.put(path, httpHandler);
        filters.put(path, filter);
        authenticatedPaths.put(path, auth);
        return this;
    }

    public BasicHttpServerBuilder withHandlerAndFilter(String path, HttpHandler httpHandler, Filter filter) {
        return withHandlerAndFilter(path, httpHandler, filter, false);
    }

    public BasicHttpServerBuilder withAuthenticator(Authenticator authenticator) {
        this.authenticator = authenticator;
        return this;
    }

    public BasicHttpServer build() throws IOException {
    	BasicHttpServer simpleHttpServer = new BasicHttpServer(port);

        for (Map.Entry entry : handlers.entrySet()) {
            String path = (String) entry.getKey();
            HttpHandler httpHandler = (HttpHandler) entry.getValue();
            Filter filter = filters.get(path);
            boolean authenticatedPath = authenticatedPaths.get(path);
            simpleHttpServer.createPathContext(path, httpHandler, filter, authenticator, authenticatedPath);
        }
        return simpleHttpServer;
    }
}
