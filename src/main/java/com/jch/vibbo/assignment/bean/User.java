package com.jch.vibbo.assignment.bean;

import java.util.ArrayList;
import java.util.List;


public class User {

	private String userName;
    private String password;
    private List<String> roles = new ArrayList<>();
    
    
	public User(String userName, String password, List<String> roles) {
		this.userName = userName;
		this.password = password;
		this.roles = roles;
	}
	
	//username=jch&password=19021988&roles=PAGE_1
	public User(String requestString) {
		String[] arrRequest = requestString.split("&");
		for(String strAtt : arrRequest){
			if(strAtt.toLowerCase().startsWith("username")){
				this.userName = getStrValue(strAtt);
			}else if(strAtt.toLowerCase().startsWith("password")){
				this.password = getStrValue(strAtt);
			}else if(strAtt.toLowerCase().startsWith("roles")){
				if(getStrValue(strAtt).contains(",")){
					for(String str : getStrValue(strAtt).split(",")){
						this.roles.add(str);
					}
				}else{
					this.roles.add(getStrValue(strAtt));
				}
			}
		}
	}

	private String getStrValue(String strAtt) {
		return strAtt.substring(strAtt.indexOf("=") + 1, strAtt.length());
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the roles
	 */
	public List<String> getRoles() {
		return roles;
	}
	/**
	 * @param roles the roles to set
	 */
	public void setRoles(ArrayList<String> roles) {
		this.roles = roles;
	}
    
	@Override
	public String toString() {
		return "username : " + this.getUserName() + " Roles : " + String.join(",", this.getRoles());
	}
    
}
