package com.jch.vibbo.assignment.repository.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.jch.vibbo.assignment.bean.RolesUser;
import com.jch.vibbo.assignment.bean.User;
import com.jch.vibbo.assignment.repository.UserRepository;

public class UserRepositoryLocal implements UserRepository{

	
	private ConcurrentMap<String, User> userTable;
	
	
	public UserRepositoryLocal() {
		userTable = new  ConcurrentHashMap<>();
		fillUserTable();
	}

	@Override
	public void insert(User user) {
		if (!userTable.containsKey(user.getUserName())) {
            userTable.put(user.getUserName(), user);
        }
	}

	@Override
	public void update(User user) {
		User existingUser = userTable.get(user.getUserName());
		if(existingUser != null){
			userTable.put(user.getUserName(), user);
		}
		
	}

	@Override
	public void delete(String userName) {
		userTable.remove(userName);
		
	}

	@Override
	public User findByUserName(String userName) {
		return userTable.get(userName);
	}
	
	private void fillUserTable() {
        this.insert(new User("user1", "user1", new ArrayList<>(Arrays.asList(RolesUser.PAGE_1.toString()))));
        this.insert(new User("user2", "user2", new ArrayList<>(Arrays.asList(RolesUser.PAGE_2.toString()))));
        this.insert(new User("user3", "user3", new ArrayList<>(Arrays.asList(RolesUser.PAGE_3.toString()))));
        this.insert(new User("user12", "user12", new ArrayList<>(Arrays.asList(RolesUser.PAGE_1.toString(),RolesUser.PAGE_2.toString()))));
        this.insert(new User("user23", "user23", new ArrayList<>(Arrays.asList(RolesUser.PAGE_2.toString(),RolesUser.PAGE_3.toString()))));
        this.insert(new User("admin", "admin", new ArrayList<>(Arrays.asList(RolesUser.ADMIN.toString(),RolesUser.PAGE_1.toString(),RolesUser.PAGE_2.toString(),RolesUser.PAGE_3.toString()))));
    }
}
