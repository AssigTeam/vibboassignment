package com.jch.vibbo.assignment.repository;

import com.jch.vibbo.assignment.bean.User;

public interface UserRepository {

    void insert(User user);

    void update(User user);

    void delete(String userName);

    User findByUserName(String userName);
}
