package com.jch.vibbo.assignment.utils;

public enum HTTPMethods {
	PUT, GET, POST, DELETE
}
