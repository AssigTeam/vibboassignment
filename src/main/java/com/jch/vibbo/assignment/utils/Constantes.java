package com.jch.vibbo.assignment.utils;

public interface Constantes {

	/**********
	 Pages
	 **********/
	
	final String LOGIN = "/login";
	final String LOGGED = "/logged";
	final String LOGOUT = "/logout";
	final String AUTHENTICATE = "/authenticate";
	final String PAGE_1 = "/page1";
	final String PAGE_2 = "/page2";
	final String PAGE_3 = "/page3";
	final String USER_ADMIN = "/UserAdmin";
	final String USER_ADMIN_DELETE = "/deleteUser";
	final String USER_ADMIN_UPDATE = "/updateUser";
	final String USER_ADMIN_INSERT = "/insertUser";
	final String USER_ADMIN_ACTION = "/adminAction";
	final String USER_INFO_USER = "/info";
	final String USER_GET_INFO = "/infoUser";
	
	/**************
	 * Attributes
	 **************/
	
	final String ATTR_USER_CONNECTED = "ATTR_USER_CONNECTED";
	final String ATTR_SESSION_TOKEN = "ATTR_SESSION_TOKEN";
	final String ATTR_PARAMETERS_MAP = "ATTR_PARAMETERS_MAP";
	final String CONTENT_TYPE_HEADER = "Content-Type";
    final String APPLICATION_JSON = "application/json";
	
	/***********
	 * Session
	 **********/
	final String COOKIE_HEADER = "Cookie";
    final String EQUAL = "=";
    final String SESSION_KEY = "session";
    final String LOCATION_HEADER = "Location";
    final String WEB_SERVER_LOGIN_URL = "http://localhost:9000/login";
    final String INC_PARAM = "?inc=";
}
