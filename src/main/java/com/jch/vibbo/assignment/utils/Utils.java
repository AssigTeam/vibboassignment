package com.jch.vibbo.assignment.utils;

import com.sun.net.httpserver.HttpExchange;

public class Utils implements Constantes{

	public static String getUserNameOfSession(HttpExchange httpExchange) {
        Object userName = httpExchange.getAttribute(ATTR_USER_CONNECTED);
        return userName != null ? userName.toString() : null;
    }
	
	public static String getSessionToken(HttpExchange httpExchange) {
        Object sessionToken = httpExchange.getAttribute(ATTR_SESSION_TOKEN);
        return sessionToken != null ? sessionToken.toString() : null;
    }
}
