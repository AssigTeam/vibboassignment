package com.jch.vibbo.assignment.factory;

import com.jch.vibbo.assignment.handler.AdminActionHandler;
import com.jch.vibbo.assignment.handler.AdminPageHandler;
import com.jch.vibbo.assignment.handler.AuthenticateHandler;
import com.jch.vibbo.assignment.handler.GetHandler;
import com.jch.vibbo.assignment.handler.InfoUserHandler;
import com.jch.vibbo.assignment.handler.LogOutHandler;
import com.jch.vibbo.assignment.handler.LoggedHandler;
import com.jch.vibbo.assignment.handler.LoginHandler;
import com.jch.vibbo.assignment.handler.Page1Handler;
import com.jch.vibbo.assignment.handler.Page2Handler;
import com.jch.vibbo.assignment.handler.Page3Handler;
import com.jch.vibbo.assignment.service.SessionService;
import com.jch.vibbo.assignment.service.UserService;
import com.jch.vibbo.assignment.utils.Constantes;

/**
 * Factory to create the handler required with the correct args.
 * 
 * @author Jérémy
 *
 */
public class HandlerFactory implements Constantes{
	/**
	 * Method use to return the rigth instance of the httpHandler needed.
	 * @param filePath path to the file
	 * @return Extractor data instance
	 */
	public GetHandler getHandler(String page, Object service) {

		switch (page) {
		case PAGE_1:
			return new Page1Handler((UserService) service);
		case PAGE_2:
			return new Page2Handler((UserService) service);
		case PAGE_3:
			return new Page3Handler((UserService) service);
		case LOGIN:
			return new LoginHandler();
		case AUTHENTICATE:
			return new AuthenticateHandler((SessionService) service);
		case LOGOUT:
			return new LogOutHandler((SessionService) service);
		case LOGGED:
			return new LoggedHandler();
		case USER_ADMIN_ACTION:
			return new AdminActionHandler((UserService) service);
		case USER_ADMIN_DELETE:
			return new AdminPageHandler((UserService) service);
		case USER_ADMIN_UPDATE:
			return new AdminPageHandler((UserService) service);
		case USER_ADMIN_INSERT:
			return new AdminPageHandler((UserService) service);
		case USER_INFO_USER:
			return new AdminPageHandler((UserService) service);
		case USER_GET_INFO:
			return new InfoUserHandler();
		default:
			break;
		}
		return null;
	}
}
