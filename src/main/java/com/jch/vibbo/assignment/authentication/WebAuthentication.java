package com.jch.vibbo.assignment.authentication;

import com.jch.vibbo.assignment.service.UserService;
import com.sun.net.httpserver.BasicAuthenticator;
import com.sun.net.httpserver.HttpExchange;

/**
 * Custom authenticator to providde security to the application.
 * 
 * @author Jérémy
 *
 */
public class WebAuthentication extends BasicAuthenticator {
	
	private UserService userService;

    public WebAuthentication(String s, UserService userService) {
        super(s);
        this.userService = userService;
    }

    @Override
    public boolean checkCredentials(String user, String pwd) {
        return userService.authenticateUser(user, pwd);
    }

    @Override
    public Result authenticate(HttpExchange var1) {
        Result result = super.authenticate(var1);
        var1.getResponseHeaders().remove("WWW-Authenticate");
        return result;
    }
}

