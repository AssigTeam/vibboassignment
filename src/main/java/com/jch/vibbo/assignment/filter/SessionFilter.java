package com.jch.vibbo.assignment.filter;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpStatus;

import com.jch.vibbo.assignment.service.SessionService;
import com.jch.vibbo.assignment.utils.Constantes;
import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.HttpExchange;

/**
 * Filter about Session. This class will allow the application to manage session.
 * Get and return information in session (token, user) or forward to login if no user in session. keeping the parameter with incoming (inc)
 * @author Jérémy
 *
 */
public class SessionFilter extends Filter implements Constantes{

    private SessionService sessionService;

    public SessionFilter(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @Override
    public void doFilter(HttpExchange httpExchange, Chain chain) throws IOException {
        List<String> cookies = httpExchange.getRequestHeaders().get(COOKIE_HEADER);

        String token = getSessionTokenFromCookies(cookies);
        String userName = sessionService.getUserNameOfSession(token);

        if (userName != null) {
            httpExchange.setAttribute(ATTR_USER_CONNECTED, userName);
            httpExchange.setAttribute(ATTR_SESSION_TOKEN, token);
            chain.doFilter(httpExchange);
        } else {
            httpExchange.getResponseHeaders().add(LOCATION_HEADER, WEB_SERVER_LOGIN_URL + INC_PARAM + httpExchange.getRequestURI());
            httpExchange.sendResponseHeaders(HttpStatus.SC_MOVED_TEMPORARILY, -1L);
        }
    }

    String getSessionTokenFromCookies(List<String> cookies) {
        String sessionToken = null;
        if (cookies != null) {
            for (String cookie : cookies) {
                sessionToken = getSessionTokenFromCookie(cookie);
                if (sessionToken != null) break;
            }
        }
        return sessionToken;
    }

    String getSessionTokenFromCookie(String cookie) {
        String sessionToken = null;
        if (cookie != null) {
            int index = cookie.indexOf(EQUAL);
            if (index > 0) {
                String key = cookie.substring(0, index);
                if (key.equals(SESSION_KEY)) {
                    sessionToken = cookie.substring(index + 1);
                }
            }
        }
        return sessionToken;
    }

    @Override
    public String description() {
        return null;
    }

}
