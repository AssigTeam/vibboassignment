package com.jch.vibbo.assignment;

import java.io.IOException;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.Authenticator;
import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class BasicHttpServer {
	private HttpServer server;

    public BasicHttpServer(int port) throws IOException {
        server = HttpServer.create(new InetSocketAddress(port), 0);

        server.setExecutor(null);
    }

    public void start() {
        server.start();
    }

    public void createPathContext(String path, HttpHandler httpHandler, Filter filter, Authenticator authenticator, boolean authenticatedPath) {
        HttpContext context = server.createContext(path, httpHandler);

        if (filter != null) {
            context.getFilters().add(filter);
        }
        if (authenticatedPath && authenticator != null) {
            context.setAuthenticator(authenticator);
        }
    }
}
