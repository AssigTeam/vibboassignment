package com.jch.vibbo.assignment.filter;

import java.io.IOException;
import java.util.Arrays;

import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.jch.vibbo.assignment.service.SessionService;
import com.jch.vibbo.assignment.service.impl.SessionGuavaServiceImpl;
import com.sun.net.httpserver.Filter.Chain;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

public class SessionFilterTest {

	private SessionFilter sessionFilter;
	
	
	/**
	 * Check get session from cookie
	 */
	@Test
	public void getSessionFromACookie(){
		sessionFilter = new SessionFilter(new SessionGuavaServiceImpl());
		Assert.assertEquals("token", sessionFilter.getSessionTokenFromCookie("session=token"));
		Assert.assertNull(sessionFilter.getSessionTokenFromCookie(null));
		Assert.assertNull(sessionFilter.getSessionTokenFromCookie("token=Sessions"));
	}
	
	/**
	 * Check get session from list of cookie only first session attr
	 */
	@Test
	public void getSessionFromListCookie(){
		sessionFilter = new SessionFilter(new SessionGuavaServiceImpl());
		Assert.assertNull("token", sessionFilter.getSessionTokenFromCookies(null));
		Assert.assertEquals("token",sessionFilter.getSessionTokenFromCookies(Arrays.asList("session=token")));
		Assert.assertEquals("token",sessionFilter.getSessionTokenFromCookies(Arrays.asList("session=token","token=Sessions")));
		Assert.assertEquals("token",sessionFilter.getSessionTokenFromCookies(Arrays.asList("session=token","token=Sessions","session=token2")));
	}
	
	/**
	 * Test Session valid and have to call chain filter.
	 * @throws IOException
	 */
	@Test
	public void testValidSessions() throws IOException{
		SessionService sessionService = Mockito.mock(SessionService.class);
		sessionFilter = new SessionFilter(sessionService);
		Mockito.when(sessionService.getUserNameOfSession("token")).thenReturn("test");
		
		Headers header = new Headers();
		header.put(sessionFilter.COOKIE_HEADER, Arrays.asList("session=token", "test=test"));
		HttpExchange exchange = Mockito.mock(HttpExchange.class);
		Mockito.when(exchange.getRequestHeaders()).thenReturn(header);
		Chain chain = Mockito.mock(Chain.class);
		sessionFilter.doFilter(exchange, chain);
		Mockito.verify(chain, Mockito.times(1)).doFilter(exchange);
		
	}
	
	/**
	 * Test Session valid and have to redirect, user is null.
	 * @throws IOException
	 */
	@Test
	public void testValidSessionsButRedirect() throws IOException{
		SessionService sessionService = Mockito.mock(SessionService.class);
		sessionFilter = new SessionFilter(sessionService);
		Mockito.when(sessionService.getUserNameOfSession("token")).thenReturn(null);
		
		Headers header = new Headers();
		header.put(sessionFilter.COOKIE_HEADER, Arrays.asList("session=token", "test=test"));
		HttpExchange exchange = Mockito.mock(HttpExchange.class);
		Mockito.when(exchange.getRequestHeaders()).thenReturn(header);
		Mockito.when(exchange.getResponseHeaders()).thenReturn(header);
		Chain chain = Mockito.mock(Chain.class);
		sessionFilter.doFilter(exchange, chain);
		Mockito.verify(chain, Mockito.times(0)).doFilter(exchange);
		Mockito.verify(exchange, Mockito.times(1)).sendResponseHeaders(HttpStatus.SC_MOVED_TEMPORARILY, -1L);
		
	}
}
