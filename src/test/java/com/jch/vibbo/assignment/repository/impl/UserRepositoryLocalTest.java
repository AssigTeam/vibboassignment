package com.jch.vibbo.assignment.repository.impl;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.jch.vibbo.assignment.bean.RolesUser;
import com.jch.vibbo.assignment.bean.User;
import com.jch.vibbo.assignment.repository.impl.UserRepositoryLocal;



/**
 * Check the userRepository methods
 * @author Jérémy
 *
 */
public class UserRepositoryLocalTest {

	private UserRepositoryLocal userRepository;
	
	@Before
	public void init() {
		userRepository = new UserRepositoryLocal();
    }

	/**
	 * Check if a user is correctly add
	 */
	@Test
	public void checkUserIsCorrectlyInserted(){
		User user = new User("test", "testpwd", new ArrayList<>(Arrays.asList(RolesUser.PAGE_1.name())));
		userRepository.insert(user);
		Assert.assertEquals(user, userRepository.findByUserName("test"));
	}
	
	/**
	 * Check if a user is correctly updated
	 */
	@Test
	public void checkUserIsCorrectlyUpdated(){
		User user = new User("test", "testpwd", new ArrayList<>(Arrays.asList(RolesUser.PAGE_1.name())));
		userRepository.insert(user);
		user.setPassword("pwdPass");
		userRepository.update(user);
		Assert.assertEquals("pwdPass", user.getPassword());
	}
	
	/**
	 * Check if a user is correctly deleted
	 */
	@Test
	public void checkUserIsCorrectlyDeleted(){
		User user = new User("test", "testpwd", new ArrayList<>(Arrays.asList(RolesUser.PAGE_1.name())));
		userRepository.insert(user);
		Assert.assertNotNull(userRepository.findByUserName("test"));
		userRepository.delete(user.getUserName());
		Assert.assertNull(userRepository.findByUserName("test"));
	}    
}
