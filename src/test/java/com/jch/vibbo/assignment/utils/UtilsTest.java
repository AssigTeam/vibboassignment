package com.jch.vibbo.assignment.utils;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.sun.net.httpserver.HttpExchange;

public class UtilsTest {

	/**
	 * Test token retreiving in existing session
	 */
	@Test
	public void retreiveTokenExistingSession(){
		HttpExchange httpExchange = Mockito.mock(HttpExchange.class);
		Mockito.when(httpExchange.getAttribute(Constantes.ATTR_SESSION_TOKEN)).thenReturn("token");
		Assert.assertEquals("token", Utils.getSessionToken(httpExchange));
	}
	
	/**
	 * Test token retreiving in non existing session
	 */
	@Test
	public void retreiveTokenNoneExistingSession(){
		HttpExchange httpExchange = Mockito.mock(HttpExchange.class);
		Mockito.when(httpExchange.getAttribute(Constantes.ATTR_SESSION_TOKEN)).thenReturn(null);
		Assert.assertNull(Utils.getSessionToken(httpExchange));
	}
	
	/**
	 * Test name retreiving in existing session
	 */
	@Test
	public void retreiveUserNameExistingSession(){
		HttpExchange httpExchange = Mockito.mock(HttpExchange.class);
		Mockito.when(httpExchange.getAttribute(Constantes.ATTR_USER_CONNECTED)).thenReturn("jérémy");
		Assert.assertEquals("jérémy", Utils.getUserNameOfSession(httpExchange));
	}
	
	/**
	 * Test name retreiving in non existing session
	 */
	@Test
	public void retreiveUserNameNoneExistingSession(){
		HttpExchange httpExchange = Mockito.mock(HttpExchange.class);
		Mockito.when(httpExchange.getAttribute(Constantes.ATTR_USER_CONNECTED)).thenReturn(null);
		Assert.assertNull(Utils.getUserNameOfSession(httpExchange));
	}
}
