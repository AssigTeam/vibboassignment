package com.jch.vibbo.assignment.service.impl;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.jch.vibbo.assignment.bean.RolesUser;
import com.jch.vibbo.assignment.bean.User;
import com.jch.vibbo.assignment.repository.UserRepository;
import com.jch.vibbo.assignment.utils.HTTPMethods;


public class UserServiceImplTest {
	
	public static final String TEST_USER_NAME = "testerName";
	public static final String TEST_USER_PWD = "123456789";
	private UserServiceImpl userService;
	
	/**
	 *  Check Role PAGE1 is only for PAGE1
	 */
	@Test
	public void checkRolesPage1(){
		UserRepository userRepo = Mockito.mock(UserRepository.class);
		Mockito.when(userRepo.findByUserName(TEST_USER_NAME)).thenReturn(new User(TEST_USER_NAME, TEST_USER_PWD, new ArrayList<>(Arrays.asList(RolesUser.PAGE_1.name()))));
		userService = new UserServiceImpl(userRepo);
		
		Assert.assertEquals(true, userService.validateUserRole(TEST_USER_NAME, RolesUser.PAGE_1));
		Assert.assertEquals(false, userService.validateUserRole(TEST_USER_NAME, RolesUser.PAGE_2));
		Assert.assertEquals(false, userService.validateUserRole(TEST_USER_NAME, RolesUser.PAGE_3));
		Assert.assertEquals(false, userService.validateUserRole(TEST_USER_NAME, RolesUser.ADMIN));
	}
	
	/**
	 *  Check Role PAGE2 is only for PAGE2
	 */
	@Test
	public void checkRolesPage2(){
		UserRepository userRepo = Mockito.mock(UserRepository.class);
		Mockito.when(userRepo.findByUserName(TEST_USER_NAME)).thenReturn(new User(TEST_USER_NAME, TEST_USER_PWD, new ArrayList<>(Arrays.asList(RolesUser.PAGE_2.name()))));
		userService = new UserServiceImpl(userRepo);
		
		Assert.assertEquals(false, userService.validateUserRole(TEST_USER_NAME, RolesUser.PAGE_1));
		Assert.assertEquals(true, userService.validateUserRole(TEST_USER_NAME, RolesUser.PAGE_2));
		Assert.assertEquals(false, userService.validateUserRole(TEST_USER_NAME, RolesUser.PAGE_3));
		Assert.assertEquals(false, userService.validateUserRole(TEST_USER_NAME, RolesUser.ADMIN));
	}
	
	/**
	 *  Check Role PAGE3 is only for PAGE3
	 */
	@Test
	public void checkRolesPage3(){
		UserRepository userRepo = Mockito.mock(UserRepository.class);
		Mockito.when(userRepo.findByUserName(TEST_USER_NAME)).thenReturn(new User(TEST_USER_NAME, TEST_USER_PWD, new ArrayList<>(Arrays.asList(RolesUser.PAGE_3.name()))));
		userService = new UserServiceImpl(userRepo);
		
		Assert.assertEquals(false, userService.validateUserRole(TEST_USER_NAME, RolesUser.PAGE_1));
		Assert.assertEquals(false, userService.validateUserRole(TEST_USER_NAME, RolesUser.PAGE_2));
		Assert.assertEquals(true, userService.validateUserRole(TEST_USER_NAME, RolesUser.PAGE_3));
		Assert.assertEquals(false, userService.validateUserRole(TEST_USER_NAME, RolesUser.ADMIN));
	}
	
	/**
	 *  Check Role PAGE1 PAGE3 is only for PAGE1 and PAGE3
	 */
	@Test
	public void checkRolesPage1_3(){
		UserRepository userRepo = Mockito.mock(UserRepository.class);
		Mockito.when(userRepo.findByUserName(TEST_USER_NAME)).thenReturn(new User(TEST_USER_NAME, TEST_USER_PWD, new ArrayList<>(Arrays.asList(RolesUser.PAGE_1.name(),RolesUser.PAGE_3.name()))));
		userService = new UserServiceImpl(userRepo);
		
		Assert.assertEquals(true, userService.validateUserRole(TEST_USER_NAME, RolesUser.PAGE_1));
		Assert.assertEquals(false, userService.validateUserRole(TEST_USER_NAME, RolesUser.PAGE_2));
		Assert.assertEquals(true, userService.validateUserRole(TEST_USER_NAME, RolesUser.PAGE_3));
		Assert.assertEquals(false, userService.validateUserRole(TEST_USER_NAME, RolesUser.ADMIN));
	}
	
	/**
	 *  Check Role PAGE2 PAGE3 is only for PAGE2 and PAGE3
	 */
	@Test
	public void checkRolesPage2_3(){
		UserRepository userRepo = Mockito.mock(UserRepository.class);
		Mockito.when(userRepo.findByUserName(TEST_USER_NAME)).thenReturn(new User(TEST_USER_NAME, TEST_USER_PWD, new ArrayList<>(Arrays.asList(RolesUser.PAGE_2.name(),RolesUser.PAGE_3.name()))));
		userService = new UserServiceImpl(userRepo);
		
		Assert.assertEquals(false, userService.validateUserRole(TEST_USER_NAME, RolesUser.PAGE_1));
		Assert.assertEquals(true, userService.validateUserRole(TEST_USER_NAME, RolesUser.PAGE_2));
		Assert.assertEquals(true, userService.validateUserRole(TEST_USER_NAME, RolesUser.PAGE_3));
		Assert.assertEquals(false, userService.validateUserRole(TEST_USER_NAME, RolesUser.ADMIN));
	}
	
	/**
	 *  Check Role PAGE2 PAGE1 is only for PAGE2 and PAGE1
	 */
	@Test
	public void checkRolesPage1_2(){
		UserRepository userRepo = Mockito.mock(UserRepository.class);
		Mockito.when(userRepo.findByUserName(TEST_USER_NAME)).thenReturn(new User(TEST_USER_NAME, TEST_USER_PWD, new ArrayList<>(Arrays.asList(RolesUser.PAGE_1.name(),RolesUser.PAGE_2.name()))));
		userService = new UserServiceImpl(userRepo);
		
		Assert.assertEquals(true, userService.validateUserRole(TEST_USER_NAME, RolesUser.PAGE_1));
		Assert.assertEquals(true, userService.validateUserRole(TEST_USER_NAME, RolesUser.PAGE_2));
		Assert.assertEquals(false, userService.validateUserRole(TEST_USER_NAME, RolesUser.PAGE_3));
		Assert.assertEquals(false, userService.validateUserRole(TEST_USER_NAME, RolesUser.ADMIN));
	}
	
	/**
	 *  Check Role ADMIN is only for allPages + admin
	 */
	@Test
	public void checkRolesAdmin(){
		UserRepository userRepo = Mockito.mock(UserRepository.class);
		Mockito.when(userRepo.findByUserName(TEST_USER_NAME)).thenReturn(new User(TEST_USER_NAME, TEST_USER_PWD, new ArrayList<>(Arrays.asList(RolesUser.ADMIN.name()))));
		userService = new UserServiceImpl(userRepo);
		
		Assert.assertEquals(true, userService.validateUserRole(TEST_USER_NAME, RolesUser.PAGE_1));
		Assert.assertEquals(true, userService.validateUserRole(TEST_USER_NAME, RolesUser.PAGE_2));
		Assert.assertEquals(true, userService.validateUserRole(TEST_USER_NAME, RolesUser.PAGE_3));
		Assert.assertEquals(true, userService.validateUserRole(TEST_USER_NAME, RolesUser.ADMIN));
	}
	
	/**
	 *  Check Role PAGE1 allow only get
	 */
	@Test
	public void checkOperationsRightP1(){
		UserRepository userRepo = Mockito.mock(UserRepository.class);
		Mockito.when(userRepo.findByUserName(TEST_USER_NAME)).thenReturn(new User(TEST_USER_NAME, TEST_USER_PWD, new ArrayList<>(Arrays.asList(RolesUser.PAGE_1.name()))));
		userService = new UserServiceImpl(userRepo);

		Assert.assertEquals(true, userService.validateUserOperation(TEST_USER_NAME, HTTPMethods.GET.name()));
		Assert.assertEquals(false, userService.validateUserOperation(TEST_USER_NAME, HTTPMethods.POST.name()));
		Assert.assertEquals(false, userService.validateUserOperation(TEST_USER_NAME, HTTPMethods.PUT.name()));
		Assert.assertEquals(false, userService.validateUserOperation(TEST_USER_NAME, HTTPMethods.DELETE.name()));
	}
	
	/**
	 *  Check Role PAGE2 allow only get
	 */
	@Test
	public void checkOperationsRightP2(){
		UserRepository userRepo = Mockito.mock(UserRepository.class);
		Mockito.when(userRepo.findByUserName(TEST_USER_NAME)).thenReturn(new User(TEST_USER_NAME, TEST_USER_PWD, new ArrayList<>(Arrays.asList(RolesUser.PAGE_2.name()))));
		userService = new UserServiceImpl(userRepo);

		Assert.assertEquals(true, userService.validateUserOperation(TEST_USER_NAME, HTTPMethods.GET.name()));
		Assert.assertEquals(false, userService.validateUserOperation(TEST_USER_NAME, HTTPMethods.POST.name()));
		Assert.assertEquals(false, userService.validateUserOperation(TEST_USER_NAME, HTTPMethods.PUT.name()));
		Assert.assertEquals(false, userService.validateUserOperation(TEST_USER_NAME, HTTPMethods.DELETE.name()));
	}
	
	/**
	 *  Check Role PAGE3 allow only get
	 */
	@Test
	public void checkOperationsRightP3(){
		UserRepository userRepo = Mockito.mock(UserRepository.class);
		Mockito.when(userRepo.findByUserName(TEST_USER_NAME)).thenReturn(new User(TEST_USER_NAME, TEST_USER_PWD, new ArrayList<>(Arrays.asList(RolesUser.PAGE_3.name()))));
		userService = new UserServiceImpl(userRepo);

		Assert.assertEquals(true, userService.validateUserOperation(TEST_USER_NAME, HTTPMethods.GET.name()));
		Assert.assertEquals(false, userService.validateUserOperation(TEST_USER_NAME, HTTPMethods.POST.name()));
		Assert.assertEquals(false, userService.validateUserOperation(TEST_USER_NAME, HTTPMethods.PUT.name()));
		Assert.assertEquals(false, userService.validateUserOperation(TEST_USER_NAME, HTTPMethods.DELETE.name()));
	}
	
	/**
	 *  Check Role ADMIN allow only get
	 */
	@Test
	public void checkOperationsRightAdmin(){
		UserRepository userRepo = Mockito.mock(UserRepository.class);
		Mockito.when(userRepo.findByUserName(TEST_USER_NAME)).thenReturn(new User(TEST_USER_NAME, TEST_USER_PWD, new ArrayList<>(Arrays.asList(RolesUser.ADMIN.name()))));
		userService = new UserServiceImpl(userRepo);

		Assert.assertEquals(true, userService.validateUserOperation(TEST_USER_NAME, HTTPMethods.GET.name()));
		Assert.assertEquals(true, userService.validateUserOperation(TEST_USER_NAME, HTTPMethods.POST.name()));
		Assert.assertEquals(true, userService.validateUserOperation(TEST_USER_NAME, HTTPMethods.PUT.name()));
		Assert.assertEquals(true, userService.validateUserOperation(TEST_USER_NAME, HTTPMethods.DELETE.name()));
	}
}