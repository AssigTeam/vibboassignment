package com.jch.vibbo.assignment.service.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test for Session from Google Guava
 * @author Jérémy
 *
 */
public class SessionGuavaServiceImplTest {

	private SessionGuavaServiceImpl sessionService;

	@Before
    public void init(){
    	sessionService = new SessionGuavaServiceImpl();

    }
    
	/**
	 * Check the newSession
	 */
	@Test
	public void checkSessionContent(){
		sessionService.newSession("token", "name");
		Assert.assertEquals("name", sessionService.getUserNameOfSession("token"));
	}
	
	/**
	 * Check the invalidation session
	 */
	@Test
	public void checkInvalidateSession(){
		sessionService.newSession("token", "name");
		Assert.assertEquals("name", sessionService.getUserNameOfSession("token"));
		sessionService.invalidate("token");
		Assert.assertNull(sessionService.getUserNameOfSession("token"));
	}
}
